<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Laratrust\Contracts\LaratrustUser;
use Laratrust\Traits\HasRolesAndPermissions;
use Illuminate\Database\Eloquent\SoftDeletes;
class Admin extends Authenticatable implements LaratrustUser
{
    use HasApiTokens, SoftDeletes;
    use HasRolesAndPermissions;
    use HasFactory;


    protected $fillable=['email','full_name',
    'password','is_reception', 'is_analyzer' ,'phone_number'];
    protected $hidden=['created_at','updated_at','remember_token'];





    public function doctors()
    {
        return $this->hasMany(Doctor::class);
    }
    public function patients()
    {
        return $this->hasMany(Patient::class);
    }
    public function appointment()
    {
        return $this->hasMany(Patient::class);
    }
    public function report()
    {
        return $this->hasMany(Report::class);
    }
    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }
    public function medicalCenters()
    {
        return $this->belongsToMany(MedicalCenter::class, 'admins_medical_centers');
    }
}
