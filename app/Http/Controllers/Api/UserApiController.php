<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Http\Requests\StoreAdminRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserApiController extends Controller
{
    // use ApiResponseTrait;
    //


public function index(){

// $admins = Admin::all();
// return view('backend.pages.admins.index',compact('admins'));
//  return $admins;
 $users = User::all();
 return  response([
    'users'=>$users
],200);
}

public function store(Request $request){


    $data['name']  = $request->full_name;
    $data['email'] = $request->email;
    $data['password'] = $request->password;


    $user= User::create($data);

    return response()->json([
        'status' => true,
        'message' => 'Admin Created Successfully',
        'user' => $user,
    ]);

}


public function update(Request $request,$id){

    $user= User::findOrFail($id);
    $data['name']  = $request->full_name;
    $data['email'] = $request->email;
    $data['password'] = $request->password;


    $user->update($request->all());
        return response()->json([
            'status'=>true,
            'data'=>$user,
            'message' => 'User Updated Successfully',
        ]);
}

public function destroy($id)
{
    $user = User::findOrFail($id);
        $user->delete();
    return response()->json([
        'status'=>true,
        'message' => 'Request Information deleted Successfully',
    ]);
    }
}
